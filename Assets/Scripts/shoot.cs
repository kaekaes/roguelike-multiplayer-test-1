﻿using UnityEngine;
using System.Collections;

public class shoot : MonoBehaviour {

	public Transform Adelante;
	public Transform Atras;
	public Transform Izquierda;
	public Transform Derecha;
	public GameObject bulletprefab;
	public float ShootSpeed = 3.0f;
	public float ShootSpeedRecarga;
	public float shootforce;
	public bool Disparado = false;

	void Start (){
		ShootSpeedRecarga = ShootSpeed;
	}
	void Update () {
		if(Disparado){
			ShootSpeed -= Time.deltaTime;
		}

		if (ShootSpeed <= 0) {
			ShootSpeed = ShootSpeedRecarga;
			Disparado = false;
			Debug.Log ("YEP");
		}

		if((Input.GetKey(KeyCode.UpArrow))&&(ShootSpeed == ShootSpeedRecarga)){
			DisparoAdelante();
			Disparado = true;
		}
		if((Input.GetKey(KeyCode.DownArrow))&& (ShootSpeed == ShootSpeedRecarga)){
			DisparoAtras();
			Disparado = true;
		}
		if((Input.GetKey(KeyCode.RightArrow))&& (ShootSpeed == ShootSpeedRecarga)){
			DisparoDerecha();
			Disparado = true;
		}
		if((Input.GetKey(KeyCode.LeftArrow))&& (ShootSpeed == ShootSpeedRecarga)){
			DisparoIzquierda();
			Disparado = true;
		}


	}

	void DisparoAdelante(){
		var bullet =(GameObject) Instantiate(bulletprefab ,Adelante.transform.position,Adelante.transform.rotation);
		bullet.GetComponent<Rigidbody> ().velocity = bullet.transform.forward * shootforce;
	}
	void DisparoAtras(){
		var bullet =(GameObject) Instantiate(bulletprefab ,Atras.transform.position,Atras.transform.rotation);
		bullet.GetComponent<Rigidbody> ().velocity = bullet.transform.forward * shootforce;
	}
	void DisparoDerecha(){
		var bullet =(GameObject) Instantiate(bulletprefab ,Derecha.transform.position,Derecha.transform.rotation);
		bullet.GetComponent<Rigidbody> ().velocity = bullet.transform.forward * shootforce;
	}
	void DisparoIzquierda(){
		var bullet =(GameObject) Instantiate(bulletprefab ,Izquierda.transform.position,Izquierda.transform.rotation);
		bullet.GetComponent<Rigidbody> ().velocity = bullet.transform.forward * shootforce;
	}

}